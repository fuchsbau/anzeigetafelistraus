package de.fuchspfoten.azt;

import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.chat.ComponentSerializer;
import net.minecraft.server.v1_12_R1.IChatBaseComponent;
import net.minecraft.server.v1_12_R1.IChatBaseComponent.ChatSerializer;
import net.minecraft.server.v1_12_R1.PacketPlayOutPlayerListHeaderFooter;
import org.bukkit.craftbukkit.v1_12_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import java.lang.reflect.Field;

/**
 * Helper for unsafe operations.
 */
public final class Unsafe {

    /**
     * The header field of {@link PacketPlayOutPlayerListHeaderFooter}.
     */
    private static final Field headerField;

    /**
     * The footer field of {@link PacketPlayOutPlayerListHeaderFooter}.
     */
    private static final Field footerField;

    static {
        try {
            headerField = PacketPlayOutPlayerListHeaderFooter.class.getDeclaredField("a");
            headerField.setAccessible(true);
            footerField = PacketPlayOutPlayerListHeaderFooter.class.getDeclaredField("b");
            footerField.setAccessible(true);
        } catch (final NoSuchFieldException ex) {
            throw new IllegalStateException("could not initialize unsafe operations!", ex);
        }
    }

    /**
     * Set the player list header and footer.
     *
     * @param who The target player.
     * @param header The new header.
     * @param footer The new footer.
     */
    public static void setPlayerListHeaderFooter(final Player who, final String header, final String footer) {
        final BaseComponent[] headerComps = TextComponent.fromLegacyText(header);
        final BaseComponent[] footerComps = TextComponent.fromLegacyText(footer);
        final BaseComponent headerComp = new TextComponent(headerComps);
        final BaseComponent footerComp = new TextComponent(footerComps);
        final String headerSerial = ComponentSerializer.toString(headerComp);
        final String footerSerial = ComponentSerializer.toString(footerComp);

        // Create the handles.
        final IChatBaseComponent handleHeader = ChatSerializer.b(headerSerial);
        final IChatBaseComponent handleFooter = ChatSerializer.b(footerSerial);

        // Create the packet.
        //noinspection TypeMayBeWeakened
        final PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();
        try {
            headerField.set(packet, handleHeader);
            footerField.set(packet, handleFooter);
        } catch (final IllegalAccessException e) {
            throw new IllegalStateException("Access failed", e);
        }

        // Send the packet.
        ((CraftPlayer) who).getHandle().playerConnection.sendPacket(packet);
    }

    /**
     * Private constructor to prevent instance creation.
     */
    private Unsafe() {
    }
}
