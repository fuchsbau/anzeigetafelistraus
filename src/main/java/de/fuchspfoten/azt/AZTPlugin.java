package de.fuchspfoten.azt;

import de.fuchspfoten.azt.modules.HeaderFooterModule;
import de.fuchspfoten.azt.modules.ScoreboardModule;
import lombok.Getter;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * The main plugin class.
 */
public class AZTPlugin extends JavaPlugin {

    /**
     * The singleton plugin instance.
     */
    private @Getter static AZTPlugin self;

    @Override
    public void onEnable() {
        self = this;

        // Create the default configuration.
        getConfig().options().copyDefaults(true);
        saveConfig();

        // Register command executor modules.
        // ...

        // Register modules.
        getServer().getPluginManager().registerEvents(new HeaderFooterModule(getConfig()), this);
        getServer().getPluginManager().registerEvents(new ScoreboardModule(getConfig()), this);
    }
}
