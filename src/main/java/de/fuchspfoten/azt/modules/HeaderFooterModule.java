package de.fuchspfoten.azt.modules;

import de.fuchspfoten.azt.AZTPlugin;
import de.fuchspfoten.azt.Unsafe;
import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 * A module that sets the tab list header and footer.
 */
public class HeaderFooterModule implements Listener {

    /**
     * The header.
     */
    private final String header;

    /**
     * The footer.
     */
    private final String footer;

    /**
     * Constructor.
     *
     * @param config The plugin configuration.
     */
    public HeaderFooterModule(final ConfigurationSection config) {
        header = config.getString("header");
        footer = config.getString("footer");
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(AZTPlugin.getSelf(),
                () -> Unsafe.setPlayerListHeaderFooter(event.getPlayer(), header, footer), 100L);
    }
}
