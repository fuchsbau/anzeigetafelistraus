package de.fuchspfoten.azt.modules;

import org.bukkit.Bukkit;
import org.bukkit.configuration.ConfigurationSection;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.permissions.Permissible;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;
import org.bukkit.scoreboard.Team.Option;
import org.bukkit.scoreboard.Team.OptionStatus;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

/**
 * A module that manages the global scoreboards.
 */
public class ScoreboardModule implements Listener {

    /**
     * Maps player UUIDs to scoreboards.
     */
    private final Map<UUID, Scoreboard> scoreboardMap = new HashMap<>();

    /**
     * Maps team names to prefixes.
     */
    private final Map<String, String> teamPrefixMap = new HashMap<>();

    /**
     * Maps team names to suffixes.
     */
    private final Map<String, String> teamSuffixMap = new HashMap<>();

    /**
     * Constructor.
     *
     * @param config The plugin configuration.
     */
    public ScoreboardModule(final ConfigurationSection config) {
        final ConfigurationSection ranksSection = config.getConfigurationSection("ranks");
        for (final String rank : ranksSection.getKeys(false)) {
            teamPrefixMap.put(rank, ranksSection.getString(rank + ".prefix"));
            teamSuffixMap.put(rank, ranksSection.getString(rank + ".suffix"));
        }
    }

    @EventHandler
    public void onPlayerJoin(final PlayerJoinEvent event) {
        // Create and cache a new scoreboard.
        final Scoreboard board = createScoreboard();
        event.getPlayer().setScoreboard(board);
        scoreboardMap.put(event.getPlayer().getUniqueId(), board);

        // Add existing players to the new scoreboard.
        for (final Player other : Bukkit.getOnlinePlayers()) {
            if (other == event.getPlayer()) {
                continue;
            }

            // Add to the respective team.
            final String teamIn = computeTeam(other);
            board.getTeam(teamIn).addEntry(other.getName());
        }

        // Add this player to all scoreboard teams.
        final String teamIn = computeTeam(event.getPlayer());
        if (teamIn != null) {
            for (final Scoreboard otherBoard : scoreboardMap.values()) {
                otherBoard.getTeam(teamIn).addEntry(event.getPlayer().getName());
            }
        }
    }

    @EventHandler
    public void onPlayerQuit(final PlayerQuitEvent event) {
        scoreboardMap.remove(event.getPlayer().getUniqueId());
    }

    /**
     * Creates a scoreboard.
     *
     * @return The scoreboard.
     */
    private Scoreboard createScoreboard() {
        final Scoreboard result = Bukkit.getScoreboardManager().getNewScoreboard();

        for (final Entry<String, String> prefixEntry : teamPrefixMap.entrySet()) {
            final Team team = result.registerNewTeam(prefixEntry.getKey());
            team.setPrefix(prefixEntry.getValue());
            team.setSuffix(teamSuffixMap.get(prefixEntry.getKey()));
            team.setOption(Option.NAME_TAG_VISIBILITY, OptionStatus.ALWAYS);
            team.setAllowFriendlyFire(true);
            team.setCanSeeFriendlyInvisibles(true);
        }

        return result;
    }

    /**
     * Computes the team of the given player.
     *
     * @param who The player.
     * @return The team or null.
     */
    private String computeTeam(final Permissible who) {
        for (final String teamName : teamPrefixMap.keySet()) {
            if (who.hasPermission("azt.rank." + teamName)) {
                return teamName;
            }
        }
        return null;
    }
}
